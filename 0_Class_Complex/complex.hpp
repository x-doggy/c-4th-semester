/**
 * @author Vladimir Stadnik <mailto:x-doggy atya.ru>
 * @date 2015
 *
 * Complex number realization
 */

#ifndef COMPLEX_HPP
#define COMPLEX_HPP

#include <iostream>
#include <cmath>
#include <limits>


class Complex {
    double re, im;

public:

    struct complex_zero_dividing{};

    Complex(): re(0), im(0) {}
    Complex(double _re, double _im): re(_re), im(_im) {}
    Complex(const Complex &c): re(c.re), im(c.im) {}
    Complex(Complex &&c): re(c.re), im(c.im) {}

    void setRe(double);
    void setIm(double);

    double getRe() const;
    double getIm() const;

    double norm() const;
    double abs()  const;
    double arg()  const;
    Complex conj(const Complex&) const;

    Complex& operator+=(const Complex&);
    Complex& operator-=(const Complex&);
    Complex& operator*=(const Complex&);
    Complex& operator/=(const Complex&);

    Complex operator+(const Complex&) const;
    Complex operator-(const Complex&) const;
    Complex operator*(const Complex&) const;
    Complex operator/(const Complex&) const;

    Complex& operator=(const Complex&);
    Complex& operator=(Complex&&);

    friend std::ostream& operator<<(std::ostream&, const Complex&);
    friend std::istream& operator>>(std::istream&, Complex&);

    friend bool operator==(Complex&, Complex&);
    friend bool operator!=(Complex&, Complex&);
};

#endif
