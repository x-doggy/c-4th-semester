#include "complex.hpp"
using namespace std;

int main(int argc, char **argv) {

    Complex c1;
    cout << "Enter c1: ";
    cin >> c1;
    cout << "c1 = " << c1 << "\tabs(c1) = " << c1.abs() << endl;
    cout << "Re(c1) = " << c1.getRe() << "\tIm(c1) = " << c1.getIm() << endl;

    Complex c2(1, -6);
    cout << "c2 = " << c2 << "\tabs(c2) = " << c2.abs() << endl;
    cout << "c1 != c2? - " << (c1 != c2) << endl;

    Complex c3 = c1 + c2;
    cout << "c3 = c1 + c2 = " << c3 << "\tabs(c3) = " << c3.abs() << endl;
    c3.setIm(1007);
    cout << "After set Im=1007, c3 = c1 + c2 = " << c3 << "\tabs(c3) = " << c3.abs() << endl;

    Complex c4(c2);
    cout << "c4 = c2 = " << c4 << "\tabs(c4) = " << c4.abs() << endl;
    c4.setRe(-1008);
    cout << "After set Re=-1008, c4 = c2 = " << c4 << "\tabs(c4) = " << c4.abs() << endl;

    return 0;
}
