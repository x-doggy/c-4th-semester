#include "complex.hpp"


void Complex::setRe(double _re) {
    re = _re;
}

void Complex::setIm(double _im) {
    im = _im;
}

double Complex::getRe() const {
    return re;
}

double Complex::getIm() const {
    return im;
}

double Complex::norm() const {
    return re*re + im*im;
}

double Complex::abs() const {
    return sqrt( norm() );
}

double Complex::arg() const {
    return atan(im / re);
}

Complex Complex::conj(const Complex &c) const {
    return Complex(c.re, -c.im);
}


Complex Complex::operator+(const Complex &c) const {
    return Complex(re + c.re, im + c.im);
}

Complex Complex::operator-(const Complex &c) const {
    return Complex(re - c.re, im - c.im);
}

Complex Complex::operator*(const Complex &c) const {
    return Complex(re * c.re - im * c.im, re * c.im + im * c.re);
}

Complex Complex::operator/(const Complex &c) const {
    double denom = c.re + c.im;
    if (!denom)
        throw complex_zero_dividing();

    return Complex((re*c.re + im*c.im) / denom, (re*c.im - im*c.re) / denom);
}


Complex& Complex::operator+=(const Complex &c) {
    re += c.re;
    im += c.im;
    return *this;
}

Complex& Complex::operator-=(const Complex &c) {
    re -= c.re;
    im -= c.im;
    return *this;
}

Complex& Complex::operator*=(const Complex &c) {
    re *= c.re;
    im *= c.im;
    return *this;
}

Complex& Complex::operator/=(const Complex &c) {
    if (!c.re || !c.im)
        throw complex_zero_dividing();

    re /= c.re;
    im /= c.im;
    return *this;
}


Complex& Complex::operator=(const Complex &c) {
    if (this != &c) {
        re = c.re;
        im = c.im;
    }

    return *this;
}

Complex& Complex::operator=(Complex &&c) {
    if (this != &c) {
        std::swap(re, c.re);
        std::swap(im, c.im);
    }

    return *this;
}


std::ostream& operator<<(std::ostream &out, const Complex &c) {
    if (!c.re && !c.im)
        out << "0" << std::endl;
    else if (!c.re && c.im)
        out << c.im << "i" << std::endl;
    else if (c.re && !c.im)
        out << c.re << std::endl;
    else
        out << c.re << (c.im>0 ? "+" : "") << c.im << "i" << std::endl;

    return out;
}

std::istream& operator>>(std::istream &in, Complex &c) {
    if (!(in >> c.re >> c.im)) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        c.setRe(0);
        c.setIm(0);
    }
    return in;
}

bool operator==(Complex &compl1, Complex &compl2) {
    return (compl1.re == compl2.re && compl1.im == compl2.im);
}

bool operator!=(Complex &compl1, Complex &compl2) {
    return !(compl1 == compl2);
}
