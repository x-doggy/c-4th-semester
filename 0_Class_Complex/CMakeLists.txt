cmake_minimum_required(VERSION 3.7)
project("C++ 4th semester")
set(CMAKE_CXX_STANDARD 11)

set(SOURCE_LIB complex.cpp)
add_library(complex STATIC ${SOURCE_LIB})

set(SOURCE_FILES main.cpp)

add_executable(0_Complex ${SOURCE_FILES})
target_link_libraries(0_Complex complex)
