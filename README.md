# C++ 4 семестр.

Программы адаптированы для [GNU/Linux](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/) и написаны там же.

Эти `system("cls")` и `system("pause")`, так нужные для работы из-под вашей винды, остаются на вашей совести.

Для кросс-платформенной сборки используется [CMake](https://cmake.org/).
Благодаря этому проект легко импортируется в [CLion](https://www.jetbrains.com/clion/).

## Сборка всего проекта

Чтобы собрать весь проект из-под `Unix` нужно с помощью `cd` перейти в папку с этим проектом и выполнить:

```
cmake -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles"
```

А далее стандартный:

```
make
```

## Задания

Задания описаны в файле [second.txt](doc/second.txt). Мои номера: 5, 13, 25.

## Мои советы

Да, я совершенно не умею правильно писать C++-классы, как бы я не старался.

Возможно, и вы тоже испытываете сложности, я вас понимаю.

Послушайте что сказал великий Линус Торвальдс о C++: [LWN.net](https://lwn.net/Articles/249460/)

Короч, пацаны, в реальной жизни пишите на C, PHP и JavaScript и будет всем счастье!
