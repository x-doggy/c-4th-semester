/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 31.07.2015
 *
 * Binary tree implementation, including:
 *    @class node_t node of a tree
 *    @class bintree_t binary tree of search
 *    @class base_iterator base of following iterators
 *    @class tree_iterator iterator for binary tree
 *    @class tree_const_iterator constant iterator for binary tree
 */

#ifndef TEMPLATE_CLASS_BINARY_TREE_H
#define TEMPLATE_CLASS_BINARY_TREE_H

#include <limits>
#include <stack>
#include "triple.hpp"


inline static void clear_stream(std::istream &in) {
    in.clear();
    in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}


template <class data_t>
struct node_t {
    data_t data;
    node_t<data_t> *left, *right;
    node_t(data_t const &Data = data_t(),
         node_t<data_t> *Left  = nullptr,
         node_t<data_t> *Right = nullptr):
            data(Data), left(Left), right(Right)
    {}
};


template <class data_t>
class bintree_t {
    node_t<data_t> *root;

    void make_empty(node_t<data_t> *&rt) {
        if (!rt) return;
        make_empty(rt->left);
        make_empty(rt->right);
        delete rt;
        rt = nullptr;
    }

    node_t<data_t>* copy(node_t<data_t> const *rt = data_t()) {
        node_t<data_t> *newrt = nullptr;
        if (!root) return nullptr;
        try {
            newrt = new node_t<data_t>(rt->data, copy(rt->left), copy(rt->right));
            return newrt;
        } catch (std::bad_alloc const &err) {
            make_empty(newrt);
            std::cerr << err.what() << std::endl;
            throw err;
        }
    }

    static bool ins(node_t<data_t> *&rt, data_t const &x = data_t()) {
        if (!rt) {
            rt = new node_t<data_t>(x, nullptr, nullptr);
            return true;
        }
        if (x < rt->data)
            return ins(rt->left, x);
        if (x > rt->data)
            return ins(rt->right, x);
        return false;
    }

    static bool del(node_t<data_t> *&rt, data_t const &x = data_t()) {
        if (!rt) return false;
        if (x < rt->data)
            return del(rt->left, x);
        if (x > rt->data)
            return del(rt->right, x);
        // x == rt->data
        node_t<data_t> *pDel = rt;
        if (!rt->right)
            rt = rt->left;
        else if (!rt->left)
            rt = rt->right;
        else
            del2(pDel, rt->left);
        delete pDel;
        pDel = nullptr;
        return true;
    }

    static void del2(node_t<data_t> *&p, node_t<data_t> *&q) {
        if (q->right) {
            del2(p, q->right);
        } else {
            p->data = q->data;
            p = q;
            q = q->left;
        }
    }

    static void badprint(std::ostream &out, node_t<data_t> *rt) {
        if (!rt) return;
        print(out, rt->left);
        out << rt->data << "  ";
        print(out, rt->right);
    }

    static void print(std::ostream &out, node_t<data_t> *rt, int lvl = 0) {
        if (!rt) return;
        print(out, rt->left, lvl+1);
        for (int i=0; i<lvl; i++)
            out << "|--";
        out << " " << rt->data << std::endl;
        print(out, rt->right, lvl+1);
    }

public:

    struct exBinaryTreeDataNoFound{};

    bintree_t(): root(nullptr) {}
    
    ~bintree_t() {
        make_empty();
    }
    
    bintree_t(bintree_t<data_t> const &tree) {
        root = copy(tree.root);
    }
    
    bintree_t(bintree_t<data_t> &&tree) {
        bintree_t<data_t> tmp(tree);
        std::swap(tmp.root, root);
        tree.root = nullptr;
    }

    bintree_t& operator=(bintree_t<data_t> const &tree) {
        if (this != &tree) {
            bintree_t<data_t> tmp(tree);
            swap(root, tmp.root);
        }
        return *this;
    }

    bintree_t& operator=(bintree_t<data_t> &&tree) {
        if (this != &tree)
            swap(root, tree.root);
        return *this;
    }

    data_t& find(data_t const &x = data_t()) const {
        node_t<data_t> *p = root;
        while (p) {
            if (x < p->data)
                p = p->left;
            else if (x > p->data)
                p = p->right;
            else
                return p->data;
        }
        throw exBinaryTreeDataNoFound();
    }

    bool in(data_t const &x = data_t()) const {
        try {
            data_t f = find(x);
            return true;
        } catch (bintree_t<data_t>::exBinaryTreeDataNoFound) {
            return false;
        }
    }

    bool ins(data_t const &x = data_t()) {
        return ins(root, x);
    }

    bool del(data_t const &x = data_t()) {
        return del(root, x);
    }

    bool is_empty() const {
        return !root;
    }

    void make_empty() {
        make_empty(root);
    }

    node_t<data_t>*& getRoot() {
        return root;
    }

    template <class data_tt>
    friend std::ostream& operator<<(std::ostream&, bintree_t<data_tt> const&);
    
    template <class data_tt>
    friend std::istream& operator>>(std::istream&, bintree_t<data_tt>&);

    template <class data_tt>
    friend class base_iterator;
};

template <class data_tt>
std::ostream& operator<<(std::ostream &out, const bintree_t<data_tt> &rt) {
    bintree_t<data_tt>::print(out, rt.root);
    return out;
}

template <class data_tt>
std::istream& operator>>(std::istream &in, bintree_t<data_tt> &rt) {
    rt.make_empty();
    while ( !(in >> std::ws).eof() )  {
        data_tt data = data_tt();
        in >> data;
        if (in.fail() && !in.eof()) {
            in.clear();
            while (!isspace(in.get()))
                continue;
        }
        rt.ins(data);
    }
    return in;
}


// Итераторы для деревьев
template <class data_t>
class base_iterator {
public:
    struct exNoElem{};
    virtual ~base_iterator() {}
    virtual void start() = 0;
    virtual const data_t& get() = 0;
    virtual void next() = 0;
    virtual bool is_finish() = 0;
};


template <class data_t>
class tree_iterator: public base_iterator<data_t> {
    bintree_t<data_t> *pTree;
    node_t<data_t> *current;
    std::stack<node_t<data_t>*> path;

public:
    tree_iterator<data_t>(bintree_t<data_t> &tree): pTree(&tree) {
        start();
    }

    void start() {
        while (!path.empty())
            path.pop();

        if (!pTree->getRoot()) {
            current = nullptr;
            return;
        }

        for (current=pTree->getRoot(); current->left; current=current->left)
            path.push(current);
    }

    const data_t& get() {
        if (!current)
            throw typename base_iterator<data_t>::exNoElem();
        return current->data;
    }

    void set(data_t &data = data_t()) {
        if (!current)
            throw typename base_iterator<data_t>::exNoElem();
        current->data = data;
    }

    bool is_finish() {
        return !current;
    }

    void next() {
        if (!current)
            throw typename base_iterator<data_t>::exNoElem();

        if (current->right) {
            for (current=current->right; current->left; current=current->left)
                path.push(current);
            return;
        }

        // нет правого потомка, идём вверх
        if (path.empty()) {
            current = nullptr;
            return;
        }

        current = path.top();
        path.pop();
    }
    
    tree_iterator<data_t>& operator++() {
        this->next();
        return *this;
    }
    
    tree_iterator<data_t> operator++(int) {
        tree_iterator<data_t> tmp = *this;
        this->next();
        return tmp;
    }
};



// const iterator
template <class data_t>
class tree_const_iterator: public base_iterator<data_t> {
    bintree_t<data_t> *pTree;
    node_t<data_t> *current;
    std::stack<node_t<data_t>*> path;

public:
    tree_const_iterator<data_t>(bintree_t<data_t> &tree): pTree(&tree) {
        start();
    }

    void start() {
        while (!path.empty())
            path.pop();

        if (!pTree->getRoot()) {
            current = nullptr;
            return;
        }

        for (current=pTree->getRoot(); current->left; current=current->left)
            path.push(current);
    }

    const data_t& get() const {
        if (!current)
            throw typename base_iterator<data_t>::exNoElem();
        return current->data;
    }

    bool is_finish() {
        return !current;
    }

    void next() {
        if (!current)
            throw typename base_iterator<data_t>::exNoElem();

        if (current->right) {
            for (current=current->right; current->left; current=current->left)
                path.push(current);
            return;
        }

        // нет правого потомка, идём вверх
        if (path.empty()) {
            current = nullptr;
            return;
        }

        current = path.top();
        path.pop();
    }
    
    tree_iterator<data_t>& operator++() {
        this->next();
        return *this;
    }
    
    tree_iterator<data_t> operator++(int) {
        tree_iterator<data_t> tmp = *this;
        this->next();
        return tmp;
    }

};

#endif