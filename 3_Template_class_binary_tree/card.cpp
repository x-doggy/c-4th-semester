#include "card.hpp"

card_t::card_t():
    author(""), title(""), inv(0)
{}

card_t::card_t(std::string const &theAuthor, std::string const &theTitle, int theInv):
    author(theAuthor), title(theTitle), inv(theInv)
{}

card_t::card_t(char const *theAuthor, char const *theTitle, int theInv):
    author(theAuthor), title(theTitle), inv(theInv)
{}


std::ostream& operator<<(std::ostream &out, card_t const &t) {
    out << "(" << t.author << ", " << t.title << ", " << t.inv << ")";
    return out;
}

std::istream& operator>>(std::istream &in, card_t &t) {
    in >> t.author >> t.title >> t.inv;
    return in;
}


std::string const& card_t::getAuthor() const {
    return author;
}

std::string const& card_t::getTitle() const {
    return title;
}

int card_t::getInv() const {
    return inv;
}


void card_t::setAuthor(std::string const &theAuthor) {
    author = theAuthor;
}

void card_t::setAuthor(char const *theAuthor) {
    author = theAuthor;
}

void card_t::setTitle(std::string const &theTitle) {
    title = theTitle;
}

void card_t::setTitle(char const *theTitle) {
    title = theTitle;
}

void card_t::setInv(int theInv) {
    inv = theInv;
}


bool operator<(card_t const &t1, card_t const &t2) {
    return ( t1.author < t2.author );
}

bool operator>(card_t const &t1, card_t const &t2) {
    return ( t2 < t1 );
}

bool operator==(card_t const &t1, card_t const &t2) {
    return ( t1.author == t2.author );
}

bool operator!=(card_t const &t1, card_t const &t2) {
    return (!( t1 == t2 ));
}
