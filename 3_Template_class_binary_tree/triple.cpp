#include "triple.hpp"


triple_t::triple_t(): a(0), b(0), c(0) { }

triple_t::triple_t(uc_t A, uc_t B, uc_t C): a(A), b(B), c(C) { }

triple_t::triple_t(char const *str) {
    if (!str || strlen(str) < 3)
        throw exTripleBadData();
    a = (str[0] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);
    b = (str[1] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);
    c = (str[2] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);
}


void triple_t::setFirst(uc_t t) {
    a = t;
}

void triple_t::setSecond(uc_t t) {
    b = t;
}

void triple_t::setThird(uc_t t) {
    c = t;
}


uc_t triple_t::getFirst() const {
    return a;
}

uc_t triple_t::getSecond() const {
    return b;
}

uc_t triple_t::getThird() const {
    return c;
}


triple_t &triple_t::operator=(char const *str) {
    if (!str || strlen(str) < 3)
        throw exTripleBadData();

    a = (str[0] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);
    b = (str[1] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);
    c = (str[2] == '0') ? static_cast<uc_t>(0) : static_cast<uc_t>(1);

    return *this;
}


std::ostream& operator<<(std::ostream &out, triple_t const &t) {
    out << "(" << t.a << ", "
               << t.b << ", "
               << t.c << ")";
    return out;
}

std::istream& operator>>(std::istream &in, triple_t &t) {
    if (!(in >> t.a >> t.b >> t.c) ||
        !(t.a==0 || t.a==1) ||
        !(t.b==0 || t.a==1) ||
        !(t.c==0 || t.c==1)
       )
        t.a = t.b = t.c = 0;
    return in;
}


uc_t triple_t::operator[](uc_t i) const {
    if (i >= 3)
        throw exTripleBadIndex();
    uc_t const triple[3] = {a, b, c};
    return triple[i];
}

uc_t triple_t::operator[](uc_t i) {
    if (i >= 3)
        throw exTripleBadIndex();
    uc_t triple[3] = {a, b, c};
    return triple[i];
}

uc_t triple_t::operator()(uc_t i) const {
    if (i >= 3)
        throw exTripleBadIndex();
    uc_t const triple[3] = {a, b, c};
    return triple[i];
}

uc_t triple_t::operator()(uc_t i) {
    if (i >= 3)
        throw exTripleBadIndex();
    uc_t triple[3] = {a, b, c};
    return triple[i];
}


bool operator<(triple_t const &t1, triple_t const &t2) {
    char tr1[3] = {
        static_cast<char>(t1.a),
        static_cast<char>(t1.b),
        static_cast<char>(t1.c)
    };
    char tr2[3] = {
        static_cast<char>(t2.a),
        static_cast<char>(t2.b),
        static_cast<char>(t2.c)
    };
    return strcmp(tr1, tr2) < 0;
}

bool operator>(triple_t const &t1, triple_t const &t2) {
    char tr1[3] = {
        static_cast<char>(t1.a),
        static_cast<char>(t1.b),
        static_cast<char>(t1.c)
    };
    char tr2[3] = {
        static_cast<char>(t2.a),
        static_cast<char>(t2.b),
        static_cast<char>(t2.c)
    };
    return strcmp(tr1, tr2) > 0;
}

bool operator==(triple_t const &t1, triple_t const &t2) {
    char tr1[3] = {
         static_cast<char>(t1.a),
         static_cast<char>(t1.b),
         static_cast<char>(t1.c)
    };
    char tr2[3] = {
        static_cast<char>(t2.a),
        static_cast<char>(t2.b),
        static_cast<char>(t2.c)
    };
    return !strcmp(tr1, tr2);
}

bool operator!=(triple_t const &t1, triple_t const &t2) {
    return !( t1 == t2 );
}
