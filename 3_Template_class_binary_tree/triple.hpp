/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 04.08.2015
 *
 * Triple of 0 and 1 implementation.
 * `unsigned short int` was chosen as the base type, because `unsigned char`
 * that sets values from 0 to 255 works as a symbol on g++
 */


#ifndef TEMPLATE_CLASS_BINARY_TREE_TRIPLE_H
#define TEMPLATE_CLASS_BINARY_TREE_TRIPLE_H

#include <iostream>
#include <cstring>

typedef unsigned short int uc_t;

class triple_t {
public:

    struct exTriple {
        virtual const char* what() const noexcept = 0;
    };
    struct exTripleBadData: public exTriple {
        char const* what() const noexcept { return "Bad data were entered!"; }
    };
    struct exTripleBadIndex: public exTriple {
        char const* what() const noexcept { return "Bad index was entered!"; }
    };

    triple_t();
    triple_t(uc_t, uc_t, uc_t);
    triple_t(const char*);

    void setFirst(uc_t);
    void setSecond(uc_t);
    void setThird(uc_t);

    uc_t getFirst()  const;
    uc_t getSecond() const;
    uc_t getThird()  const;

    uc_t operator[](uc_t) const;
    uc_t operator[](uc_t);

    uc_t operator()(uc_t) const;
    uc_t operator()(uc_t);

    triple_t& operator=(const char*);

    friend bool operator <(triple_t const&, triple_t const&);
    friend bool operator >(triple_t const&, triple_t const&);
    friend bool operator==(triple_t const&, triple_t const&);
    friend bool operator!=(triple_t const&, triple_t const&);

    friend std::ostream& operator<<(std::ostream&, triple_t const&);
    friend std::istream& operator>>(std::istream&, triple_t&);

private:    
    uc_t a, b, c;
};


#endif
