/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 10.08.2015
 *
 * Library card implementation.
 * Nothing special, I just used std::string instead of char *
 * I think that's right.
 */


#ifndef _CARD_T_H
#define _CARD_T_H

#include <iostream>
#include <string>

class card_t {
public:
    card_t();
    card_t(std::string const&, std::string const&, int);
    card_t(char const*, char const*, int);

    std::string const& getAuthor() const;
    std::string const& getTitle()  const;
            int        getInv()    const;

    void        setAuthor(std::string const&);
    void        setAuthor(char const*);
    void        setTitle(std::string const&);
    void        setTitle(char const*);
    void        setInv(int);

    friend std::ostream& operator<<(std::ostream&, card_t const&);
    friend std::istream& operator>>(std::istream&, card_t&);

    friend bool operator <(const card_t&, const card_t&);
    friend bool operator >(const card_t&, const card_t&);
    friend bool operator==(const card_t&, const card_t&);
    friend bool operator!=(const card_t&, const card_t&);

private:    
    std::string author;    // Автор книги
    std::string title;     // Название книги
    int inv;               // Инвентарный номер книги
};

#endif