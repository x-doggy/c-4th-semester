/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 10.08.2015
 *
 * Binary tree testing program. The task 27:
 * Library card contains an informaton about book's author, book's title,
 * invantary number of book (integer number). Cards sorted by authors (main key)
 * and by titles.
 * There is a necessity to work with ordered binary tree of symbols, triples of 0 and 1,
 * library cards.
 * Triples of 0 and 1 compared with respect to lexicographic order.
 * Let's develop a set of classes to work with these objects.
 * Don't forget a couple of methods for trees:
 * * insert and delete an element into tree,
 * * output tree into a stream in order ascending by elements,
 * * input tree from a stream.
 */

#include "binary_tree.hpp"
#include "triple.hpp"
#include "card.hpp"
#include <typeinfo>
#include <cstdlib>
using namespace std;


typedef triple_t data_t;

inline static void menu() {
    cout << "------------------------" << endl
         << "1. Insert elem\n"
         << "2. Delete elem\n"
         << "3. Delete whole tree\n"
         << "4. Find elem\n"
         << "5. Input tree\n\n";
}

int main(int argc, char **argv) {
    bintree_t<data_t> tree;
    data_t data = data_t();
    tree_iterator<data_t> it(tree);
    int choice;

    do {
        cout << "Main data type: " << typeid(data_t).name() << endl;
        cout << "\n------------------------\n"
             << "Tree:\n" << tree << endl;

        menu();

        cout << "Enter menu entry: ";
        while (!(cin >> choice)) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cerr << "Incorrect input!" << endl;
            cout << "Please, repeat entering: ";
        }

        switch (choice) {

            case 1: {
                cout << "Enter data: ";
                if (!(cin >> data)) {
                    clear_stream(cin);
                    cerr << "Bad data was entered!" << endl;
                }
                bool inserted = tree.ins(data);
                cout << (inserted ? "OK" : "No K") << endl;
                break;
            }

            case 2: {
                cout << "Enter data: ";
                if (!(cin >> data)) {
                    clear_stream(cin);
                    cerr << "Bad data was entered!" << endl;
                }
                bool deleted = false;
                try {
                    deleted = tree.del(data);
                } catch (bintree_t<data_t>::exBinaryTreeDataNoFound) {
                    cerr << "Tree node is not found!" << endl;
                }
                cout << (deleted ? "OK!" : "No K!") << endl;
                break;
            }

            case 3: {
                tree.make_empty();
                break;
            }

            case 4: {
                cout << "Enter data: ";
                if (!(cin >> data)) {
                    clear_stream(cin);
                    cerr << "Bad data was entered!" << endl;
                }
                data_t fnd;
                try {
                    fnd = tree.find(data);
                    cout << "Found: " << fnd << endl;
                } catch (bintree_t<data_t>::exBinaryTreeDataNoFound) {
                    cerr << "Tree node is not found!" << endl;
                }
                break;
            }
            
            case 5: {
                cout << "Enter tree:" << endl;
                cin >> tree;
                break;
            }
            
            default: {
                choice = 6;
                break;
            }
        }

    } while (choice < 6);

    return 0;
}
