/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Class "party member and teacher".
 */


#ifndef _XPARTYTEACHER_H
#define _XPARTYTEACHER_H

#include "xteacher.hpp"
#include "xmember.hpp"

class PartyTeacher: public Teacher,
                    public PartyMember {
    String *works;    // List of works in support of the party
    int     wnum;     // Number of these works

public:
    PartyTeacher();
    virtual ~PartyTeacher();

    PartyTeacher(const String&,    // for name
                 const String&,    // for fathername
                 const String&,    // for surname
                 int,              // for year
                 const String&,    // for post
                 const String&,    // for science degree
                 const String&,    // for qualify
                 String*,          // for science works
                 int,              // for number of lines in works
                 const String&,    // for partyname
                 int,              // for join
                 int,              // for ticknum
                 String*,          // for autobiography
                 int,              // for number of autobiography
                 String*,          // for works for party
                 int);             // for number of works for party

    PartyTeacher(const Teacher&,
                 const PartyMember&,
                 String*,          // for works
                 int);             // for number of works

    PartyTeacher(const PartyTeacher&);
    PartyTeacher(PartyTeacher&&);

          String* getWorksForParty()  const;
    const int     getWorksTickNum()   const;

    void   setWorksForParty(String*, int);
    void   setWork(const String&, int);

    PartyTeacher& operator=(const PartyTeacher&);
    PartyTeacher& operator=(PartyTeacher&&);

    friend std::ostream& operator<<(std::ostream&, const PartyTeacher&);
    friend std::istream& operator>>(std::istream&, PartyTeacher&);
};


#endif
