/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Class "party member".
 */

#ifndef _X_PARTYMEMBER_H
#define _X_PARTYMEMBER_H

#include "xhuman.hpp"

class PartyMember: virtual public Human {
    String  partyname;    // Name of party
    int     join;         // Year of entry into the party
    int     ticknum;      // Number of party ticket
    int     lines;        // Number of lines in autobiography
    String *autobio;      // Autobiography

public:
    PartyMember();
    virtual ~PartyMember();

    PartyMember(const String&,    // for name
                const String&,    // for fathername
                const String&,    // for surname
                int,              // for year
                const String&,    // for partyname
                int,              // for join
                int,              // for ticknum
                String*,          // for autobiography
                int);             // for number of autobiography

    PartyMember(const Human&,
                const String&,    // for partyname
                int,              // for join
                int,              // for ticknum
                String*,          // for autobiography
                int);             // for number of autobiography

    PartyMember(const PartyMember&);
    PartyMember(PartyMember&&);

    const String& getPartyName()       const;
    const int     getJoin()            const;
    const int     getTicketNum()       const;
    const int     getAutobioLines()    const;
          String* getAutobio()         const;
    const String& getAutobioLine(int)  const;

    void setPartyName(const String&);
    void setJoin(int);
    void setTicketNum(int);
    void setAutobioLine(int, const String&);
    void setAutobio(String*, int);

    static void input_data(std::istream&, PartyMember&);    // Only private fields input

    PartyMember& operator=(const PartyMember&);
    PartyMember& operator=(PartyMember&&);

    friend std::istream& operator>>(std::istream&, PartyMember&);
    friend std::ostream& operator<<(std::ostream&, const PartyMember&);
};


#endif
