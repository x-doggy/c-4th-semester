/*
 * © 2015 Vladimir Stadnik, <mailto:x-doggy@ya.ru>, <vk.com/x_doggy>
 * Developed on Manjaro GNU/Linux
 * 
 * Implementation of class "teacher in university".
 */


#include "xpartyteacher.hpp"

PartyTeacher::PartyTeacher()
        : Teacher()
        , PartyMember()
        , wnum(1)
{
    works = new String[1];
}

PartyTeacher::~PartyTeacher() {
    delete[] works;
}

PartyTeacher::PartyTeacher(const String &theName,
                           const String &theFathername,
                           const String &theSurname,
                           int theYear,
                           const String &thePost,
                           const String &theSciDegree,
                           const String &theQaulify,
                           String *theSciWorks,
                           int theHowMany,
                           const String &thePartyName,
                           int theJoin,
                           int theTickNum,
                           String *theAutobio,
                           int theLines,
                           String *theWorks,
                           int theWnum)
        : Human(theName, theSurname, theFathername, theYear)
        , Teacher(theName, theSurname, theFathername, theYear, thePost, theSciDegree, theQaulify, theSciWorks, theHowMany)
        , PartyMember(theName, theSurname, theFathername, theYear, thePartyName, theJoin, theTickNum, theAutobio, theLines)
        , wnum(theWnum)
{
    works = new String[wnum];
    for (int i=0; i< wnum; i++)
        works[i] = theWorks[i];
}

PartyTeacher::PartyTeacher(const Teacher &teacher,
                           const PartyMember &member,
                           String *theWorks,
                           int theWnum):
        Human(teacher),
        Teacher(teacher),
        PartyMember(member),
        wnum(theWnum)
{
    works = new String[wnum];
    for (int i=0; i< wnum; i++)
        works[i] = theWorks[i];
}

PartyTeacher::PartyTeacher(const PartyTeacher &pt):
        Human(pt),
        Teacher(pt),
        PartyMember(pt),
        wnum(pt.wnum)
{
    works = new String[wnum];
    for (int i=0; i<wnum; i++)
        works[i] = pt.works[i];
}

PartyTeacher::PartyTeacher(PartyTeacher &&pt) {
    std::swap(works, pt.works);
    std::swap(wnum, pt.wnum);

    pt.works = nullptr;
}

String* PartyTeacher::getWorksForParty() const {
    return works;
}

const int PartyTeacher::getWorksTickNum() const {
    return wnum;
}

void PartyTeacher::setWorksForParty(String *theWorks, int theWnum) {
    delete[] works;

    wnum = theWnum;

    works = new String[wnum];
    for (int i=0; i< wnum; i++)
        works[i] = theWorks[i];
}

void PartyTeacher::setWork(const String &theWork, int i) {
    if (i < 0 || i >= wnum)
        throw Human::exBadNumber();

    works[i] = theWork;
}

PartyTeacher& PartyTeacher::operator=(const PartyTeacher &pt) {
    if (this != &pt) {
        delete[] works;

        wnum = pt.wnum;

        for (int i=0; i< wnum; i++)
            works[i] = pt.works[i];
    }
    return *this;
}

PartyTeacher& PartyTeacher::operator=(PartyTeacher &&pt) {
    if (this != &pt) {
        std::swap(works, pt.works);
        std::swap(wnum, pt.wnum);
        pt.works = nullptr;
    }
    return *this;
}

std::ostream& operator<<(std::ostream &out, const PartyTeacher &pt) {
    out << static_cast<const Teacher&>(pt);
    out << static_cast<const PartyMember&>(pt);

    out << "Works for party [" << pt.wnum << "]:" << std::endl;
    for (int i=0; i<pt.wnum; i++)
        out << i << ") " << pt.works[i] << std::endl;

    return out;
}

std::istream& operator>>(std::istream &in, PartyTeacher &pt) {
    in >> static_cast<Teacher&>(pt);

    delete[] pt.works;
    pt.works = nullptr;

    std::cout << "Enter number of works for party: ";
    if (!(in >> pt.wnum) || pt.wnum <= 0) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    std::cout << "Enter works for party [" << pt.wnum <<  "]:" << std::endl;
    pt.works = new String[pt.wnum];
    in.ignore();
    for (int i=0; i<pt.wnum; i++) {
        std::cout << "[" << i << "]: ";
        in >> pt.works[i];
    }

    return in;
}
