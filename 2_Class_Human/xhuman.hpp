/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Class "Human" implements human's properties, such as:
 * name, fathername, surname and year of birthday.
 */


#ifndef _XHUMAN_H
#define _XHUMAN_H

#include "../cpp_string/cpp_string.h"
#include <limits>


class Human {
    String name;
    String fathername;
    String surname;
    int    year;

public:

    struct exBadNumber{};
    virtual ~Human();

    Human() = default;
    Human(const String&,    // for name
          const String&,    // for fathername
          const String&,    // for surname
          int);             // for year

    const String& getName()       const;
    const String& getSurname()    const;
    const String& getFathername() const;
    const int     getYear()       const;

    void setName(const String&);
    void setSurname(const String&);
    void setFathername(const String&);
    void setYear(int theYear);

    friend std::ostream& operator<<(std::ostream &out, const Human &human);
    friend std::istream& operator>>(std::istream &in,  Human &human);
};

#endif
