#include "xhuman.hpp"


Human::~Human() { }

Human::Human(const String &theName,
             const String &theFathername,
             const String &theSurname,
             int   theYear = 1900): name(theName)
                                  , fathername(theFathername)
                                  , surname(theSurname)
                                  , year(theYear)
{ }

std::ostream& operator<<(std::ostream &out, const Human &human) {
    out << "name = "        << human.name       << std::endl;
    out << "surname = "     << human.surname    << std::endl;
    out << "fathername = "  << human.fathername << std::endl;
    out << "year = "        << human.year       << std::endl;

    return out;
}

std::istream& operator>>(std::istream &in, Human &human) {
    std::cout << "Enter name: ";
    in >> human.name;

    std::cout << "Enter fathername: ";
    in >> human.fathername;

    std::cout << "Enter surname: ";
    in >> human.surname;

    std::cout << "Enter year: ";
    if (!(in >> human.year)) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    return in;
}

const String& Human::getName() const {
    return name;
}

const String& Human::getSurname() const {
    return surname;
}

const String& Human::getFathername() const {
    return fathername;
}

const int Human::getYear() const {
    return year;
}


void Human::setName(const String &theName) {
    name = theName;
}

void Human::setSurname(const String &theSurname) {
    surname = theSurname;
}

void Human::setFathername(const String &theFathername) {
    fathername = theFathername;
}

void Human::setYear(int theYear) {
    year = theYear;
}
