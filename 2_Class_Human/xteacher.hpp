/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Class "teacher in university".
 */

#ifndef _XTEACHER_H
#define _XTEACHER_H

#include "xhuman.hpp"

class Teacher: virtual public Human {
    String  post;          // должность
    String  sciDegree;     // учёная степень
    String  qualify;       // специальность
    String *sciWorks;      // список научных трудов
    int     howMany;       // сколько научных трудов
public:
    Teacher();
    virtual ~Teacher();

    Teacher(const String&,    // for name
            const String&,    // for fathername
            const String&,    // for surname
            int,              // for year
            const String&,    // for post
            const String&,    // for science degree
            const String&,    // for qualify
            String*,          // for science works
            int);             // for number of lines in works

    Teacher(const Human&,
            const String&,    // for post
            const String&,    // for science degree
            const String&,    // for qualify
            String*,          // for science works
            int);             // for number of lines in works

    Teacher(const Teacher&);
    Teacher(Teacher&&);

    const String&  getPost()         const;
    const String&  getSciDegree()    const;
    const String&  getQualify()      const;
          String*  getSciWorks()     const;
    const String&  getSciWork(int)   const;
    const int      getWorksAmount()  const;

    void setPost(const String&);
    void setSciDegree(const String&);
    void setQualify(const String&);
    void setSciWorks(String*, int);
    void setSciWork(int, const String&);

    Teacher& operator=(const Teacher&);
    Teacher& operator=(Teacher&&);

    friend std::ostream& operator<<(std::ostream&, const Teacher&);
    friend std::istream& operator>>(std::istream&, Teacher&);
};


#endif
