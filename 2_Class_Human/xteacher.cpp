#include "xteacher.hpp"

Teacher::Teacher() : Human()
                   , howMany(1)
{
    sciWorks = new String[1];
}

Teacher::~Teacher() {
    delete[] sciWorks;
}

Teacher::Teacher(const String &theName,
                 const String &theFathername,
                 const String &theSurname,
                 int theYear,
                 const String &thePost,
                 const String &theSciDegree,
                 const String &theQualify,
                 String *theSciWorks,
                 int theHowMany):
        Human(theName, theFathername, theSurname, theYear),
        howMany(theHowMany)
{
    post = thePost;
    sciDegree = theSciDegree;
    qualify = theQualify;

    sciWorks = new String[howMany];
    for (int i=0; i<howMany; i++)
        sciWorks[i] = theSciWorks[i];
}

Teacher::Teacher(const Human &human,
                 const String &thePost,
                 const String &theSciDegree,
                 const String &theQualify,
                 String *theSciWorks,
                 int theHowMany):
        Human(human),
        howMany(theHowMany)
{
    post = thePost;
    sciDegree = theSciDegree;
    qualify = theQualify;

    sciWorks = new String[howMany];
    for (int i=0; i<howMany; i++)
        sciWorks[i] = theSciWorks[i];
}

Teacher::Teacher(const Teacher &teacher):
        Human(teacher),
        howMany(teacher.howMany)
{
    post = teacher.post;
    sciDegree = teacher.sciDegree;
    qualify = teacher.qualify;

    sciWorks = new String[howMany];
    for (int i=0; i<howMany; i++)
        sciWorks[i] = teacher.sciWorks[i];
}

Teacher::Teacher(Teacher &&teacher): Human(teacher) {
    std::swap(post,      teacher.post);
    std::swap(sciDegree, teacher.sciDegree);
    std::swap(qualify,   teacher.qualify);
    std::swap(sciWorks,  teacher.sciWorks);
    std::swap(howMany,   teacher.howMany);

    teacher.sciWorks  = nullptr;
    teacher.qualify   = nullptr;
    teacher.sciDegree = nullptr;
    teacher.post      = nullptr;
}

Teacher& Teacher::operator=(const Teacher &teacher) {
    if (this != &teacher) {
        delete[] sciWorks;

        howMany   = teacher.howMany;
        post      = teacher.post;
        sciDegree = teacher.sciDegree;
        qualify   = teacher.qualify;

        sciWorks = new String[howMany];
        for (int i=0; i<howMany; i++)
            sciWorks[i] = teacher.sciWorks[i];
    }
    return *this;
}

Teacher& Teacher::operator=(Teacher &&teacher) {
    if (this != &teacher) {
        std::swap(post,      teacher.post);
        std::swap(sciDegree, teacher.sciDegree);
        std::swap(qualify,   teacher.qualify);
        std::swap(sciWorks,  teacher.sciWorks);
        std::swap(howMany,   teacher.howMany);

        teacher.sciWorks  = nullptr;
        teacher.qualify   = nullptr;
        teacher.sciDegree = nullptr;
        teacher.post      = nullptr;
    }
    return *this;
}

std::ostream& operator<<(std::ostream &out, const Teacher &teacher) {
    out << static_cast<const Human&>(teacher) << std::endl;

    out << "Post: "           << teacher.post             << std::endl;
    out << "Science degree: " << teacher.sciDegree        << std::endl;
    out << "Qualify: "        << teacher.qualify          << std::endl;
    out << "Science works ["  << teacher.howMany << "]: " << std::endl;
    for (int i=0; i<teacher.howMany; i++)
        out << i << ". " << teacher.sciWorks[i] << std::endl;

    return out;
}

std::istream& operator>>(std::istream &in, Teacher &teacher) {
    in >> static_cast<Human&>(teacher);

    delete[] teacher.sciWorks;
    teacher.sciWorks = nullptr;

    std::cout << "Enter post: ";
    in >> teacher.post;

    std::cout << "Enter science degree: ";
    in >> teacher.sciDegree;

    std::cout << "Enter qualification: ";
    in >> teacher.qualify;

    std::cout << "Enter amount of science work: ";
    if (!(in >> teacher.howMany) || teacher.howMany <= 0) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    std::cout << "Enter science works [" << teacher.howMany << "]:" << std::endl;

    teacher.sciWorks = new String[teacher.howMany];
    in.ignore();
    for (int i=0; i<teacher.howMany; i++) {
        std::cout << "[" << i << "]: ";
        in >> teacher.sciWorks[i];
    }

    return in;
}

const String& Teacher::getPost() const {
    return post;
}

const String& Teacher::getSciDegree() const {
    return sciDegree;
}

const String& Teacher::getQualify() const {
    return qualify;
}

const String& Teacher::getSciWork(int i) const {
    if (i >=howMany || i < 0)
        throw Human::exBadNumber();

    return sciWorks[i];
}

String* Teacher::getSciWorks() const {
    return sciWorks;
}

const int Teacher::getWorksAmount() const {
    return howMany;
}

void Teacher::setPost(const String &thePost) {
    post = thePost;
}

void Teacher::setSciDegree(const String &theSciDegree) {
    sciDegree = theSciDegree;
}

void Teacher::setQualify(const String &theQualify) {
    qualify = theQualify;
}

void Teacher::setSciWork(int i, const String &theSciWork) {
    sciWorks[i] = theSciWork;
}

void Teacher::setSciWorks(String *theSciWorks, int theHowMany) {
    delete[] sciWorks;

    howMany = theHowMany;

    sciWorks = new String[howMany];
    for (int i=0; i<howMany; i++)
        sciWorks[i] = theSciWorks[i];
}
