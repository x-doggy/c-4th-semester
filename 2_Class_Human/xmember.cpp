#include "xmember.hpp"

PartyMember::PartyMember()
        : Human()
        , join(0)
        , ticknum(0)
        , lines(1)
{
    partyname = "";
    autobio = new String[1];
}

PartyMember::~PartyMember() {
    delete[] autobio;
    autobio = nullptr;
}

PartyMember::PartyMember(const String &theName,
                         const String &theFathername,
                         const String &theSurname,
                         int theYear,
                         const String &thePartyName,
                         int theJoin,
                         int theTickNum,
                         String *theAutobio,
                         int theLines):
        Human(theName, theSurname, theFathername, theYear),
        join(theJoin),
        ticknum(theTickNum),
        lines(theLines)
{
    partyname = thePartyName;
    autobio = new String[lines];
    for (int i=0; i<lines; i++)
        autobio[i] = theAutobio[i];
}

PartyMember::PartyMember(const Human &human,
                         const String &thePartyName,
                         int theJoin,
                         int theTickNum,
                         String *theAutobio,
                         int theLines):
        Human(human),
        join(theJoin),
        ticknum(theTickNum),
        lines(theLines)
{
    partyname = thePartyName;
    autobio = new String[lines];
    for (int i=0; i<lines; i++)
        autobio[i] = theAutobio[i];
}

PartyMember::PartyMember(const PartyMember &member):
        Human(member),
        join(member.join),
        lines(member.lines)
{
    partyname = member.partyname;
    autobio = new String[lines];
    for (int i=0; i<lines; i++)
        autobio[i] = member.autobio[i];
}

PartyMember::PartyMember(PartyMember &&member): Human(member) {
    std::swap(partyname, member.partyname);
    std::swap(lines,     member.lines);
    std::swap(join,      member.join);
    std::swap(ticknum,   member.ticknum);
    std::swap(autobio,   member.autobio);

    member.autobio   = nullptr;
    member.partyname = nullptr;
}

PartyMember& PartyMember::operator=(const PartyMember &member) {
    if (this != &member) {
        delete[] autobio;

        join      = member.join;
        ticknum   = member.ticknum;
        lines     = member.lines;
        partyname = member.partyname;

        autobio = new String[lines];
        for (int i=0; i<lines; i++)
            autobio[i] = member.autobio[i];
    }
    return *this;
}

PartyMember& PartyMember::operator=(PartyMember &&member) {
    if (this != &member) {
        std::swap(partyname, member.partyname);
        std::swap(lines,     member.lines);
        std::swap(join,      member.join);
        std::swap(ticknum,   member.ticknum);
        std::swap(autobio,   member.autobio);

        member.autobio   = nullptr;
        member.partyname = nullptr;
    }
    return *this;
}

std::ostream& operator<<(std::ostream &out, const PartyMember &member) {
    out << static_cast<const Human&>(member) << std::endl; // Human entered!

    out << "Party name: "         << member.partyname      << std::endl;
    out << "Year of joining: "    << member.join           << std::endl;
    out << "Partyticket number: " << member.ticknum        << std::endl;
    out << "Autobiography ["      << member.lines << "]: " << std::endl;

    for (int i=0; i<member.lines; i++) {
        out << i << ". " << member.autobio[i] << std::endl;
    }

    return out;
}

void PartyMember::input_data(std::istream &in, PartyMember &member) {
    delete[] member.autobio;
    member.autobio = nullptr;

    std::cout << "Enter party name: ";
    in.ignore();
    in >> member.partyname;

    std::cout << "Enter an year of joining: ";
    if (!(in >> member.join) || member.join < 0) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    std::cout << "Enter number of partyticket: ";
    if (!(in >> member.ticknum) || member.ticknum <= 0) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    std::cout << "Enter amount of lines in autobiography: ";
    if (!(in >> member.lines) || member.lines <= 0) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw Human::exBadNumber();
    }

    std::cout << "Enter autobiography [" << member.lines << "]:" << std::endl;
    member.autobio = new String[member.lines];
    in.ignore();
    for (int i=0; i<member.lines; i++) {
        std::cout << "[" << i << "]: ";
        in >> member.autobio[i];
    }
}

std::istream& operator>>(std::istream &in, PartyMember &member) {
    in >> static_cast<Human&>(member);

    PartyMember::input_data(in, member);

    return in;
}


const String& PartyMember::getPartyName() const {
    return partyname;
}

const int PartyMember::getAutobioLines() const {
    return lines;
}

const String& PartyMember::getAutobioLine(int i) const {
    if (i >= lines || i < 0)
        throw Human::exBadNumber();

    return autobio[i];
}

String* PartyMember::getAutobio() const {
    return autobio;
}

const int PartyMember::getJoin() const {
    return join;
}

const int PartyMember::getTicketNum() const {
    return ticknum;
}


void PartyMember::setPartyName(const String &thePartyName) {
    partyname = thePartyName;
}

void PartyMember::setJoin(int theJoin) {
    join = theJoin;
}

void PartyMember::setTicketNum(int theTickNum) {
    ticknum = theTickNum;
}

void PartyMember::setAutobioLine(int line, const String &autobioLine) {
    if (line < 0 || line >= lines)
        throw Human::exBadNumber();

    autobio[line] = autobioLine;
}

void PartyMember::setAutobio(String *theAutobio, int theLines) {
    if (theLines < 0 || !theAutobio)
        throw Human::exBadNumber();

    delete[] autobio;

    lines = theLines;

    autobio = new String[lines];
    for (int i=0; i<lines; i++)
        autobio[i] = theAutobio[i];
}
