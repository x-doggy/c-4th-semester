/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * 13. To implementate class "Human" including name, surname, fathername, a year
 * of birth and methods that may you to set / get values of these fields.
 *
 * To realise derived classes:
 * 1) "Teacher on university" includes fields: post, academic degree,
 *     specialty, a list of scientific publications (array of strings).
 * 2) "Party member" includes fields: name of party, a year of joining into party,
 *     number of party ticket and autobiography (array of strings).
 * 3) "Teachers - party members" (derived from 2 and 3). Additional field is
 *     a list of publications in support of the party.
 *
 * These classes must include methods for access and changing all fields.
 * Limitary sizes of dynamic arrays are set with init and doesn't grow up in future.
 */

#include "xhuman.hpp"
#include "xmember.hpp"
#include "xteacher.hpp"
#include "xpartyteacher.hpp"

using namespace std;

inline void menu() {
    cout << "\n-------------------------------------------------\n"
         << "     HUMAN:\n"
         << "1. Create human\n"
         << "Change {2.name, 3.surname, 4.fathername, 5.birthyear}\n"
         << "6. Print human\n"
         << "-------------------------------------------------\n"
         << "    TEACHER:\n"
         << "7. Create teacher\n"
         << "Change {8.name, 9.surname, 10.fathername, 11.birthyear, 12.post,\n"
         << "        13.science degree, 14.qualify, 15.science works}\n"
         << "16. Print teacher\n"
         << "-------------------------------------------------\n"
         << "  PARTY MEMBER:\n"
         << "17. Create party member\n"
         << "Change {18.name, 19.surname, 20.fathername, 21.birthyear, 22.party name,\n"
         << "        23.year of join, 24.partyticket number, 25.autobiography}\n"
         << "26. Print party member\n"
         << "-------------------------------------------------\n"
         << " TEACHER & PARTY MEMBER:\n"
         << "27. Create teacher & party member\n"
         << "Change {28.name, 29.surname, 30.fathername, 31.birthyear, 32.works for party}\n"
         << "33. Print teacher-mamba\n"
         << "-------------------------------------------------\n\n";
}

inline void clear_stream(istream &in = cin) {
    in.clear();
    in.ignore(numeric_limits<streamsize>::max(), '\n');
}


int main(int argc, char **argv) {
    int key;
    Human        human;
    Teacher      teacher;
    PartyMember  member;
    PartyTeacher pt;

    do {
        menu();

        cout << "Enter key: ";
        if (!(cin >> key)) {
            clear_stream(cin);
            cerr << "Bad key entered!" << endl;
            continue;
        }

        switch (key) {

            case 1: {
                try {
                    cin >> human;
                } catch (Human::exBadNumber) {
                    cerr << "Bad year was entered!" << endl;
                    break;
                }
                cout << "Human entered!" << endl;
                Human const who(human);
                cout << "\nWho:\n" << who;
                break;
            }

            case 2: {
                cout << "Enter new name: ";
                String name;
                cin >> name;
                human.setName(name);
                break;
            }

            case 3: {
                cout << "Enter new surname: ";
                String surname;
                cin >> surname;
                human.setSurname(surname);
                break;
            }

            case 4: {
                cout << "Enter new fathername: ";
                String fathername;
                cin >> fathername;
                human.setFathername(fathername);
                break;
            }

            case 5: {
                cout << "Enter new birthyear: ";
                int birth;
                if (!(cin >> birth) || birth < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect birthyear!" << endl;
                    break;
                }
                human.setYear(birth);
                break;
            }

            case 6: {
                cout << human << endl;
                break;
            }


            //    TEACHER!!!
            case 7: {
                try {
                    cin >> teacher;
                } catch (Human::exBadNumber) {
                    cerr << "Bad number was entered!" << endl;
                    break;
                }
                cout << "Teacher entered!" << endl;
                Teacher const who = teacher;
                cout << "\nWho:\n" << who;

                break;
            }

            case 8: {
                cout << "Enter new name: ";
                String name;
                cin >> name;
                teacher.setName(name);
                break;
            }

            case 9: {
                cout << "Enter new surname: ";
                String surname;
                cin >> surname;
                teacher.setSurname(surname);
                break;
            }

            case 10: {
                cout << "Enter new fathername: ";
                String fathername;
                cin >> fathername;
                teacher.setFathername(fathername);
                break;
            }

            case 11: {
                cout << "Enter new birthyear: ";
                int birth;
                if (!(cin >> birth) || birth < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect birthyear!" << endl;
                    break;
                }
                teacher.setYear(birth);
                break;
            }

            case 12: {
                cout << "Enter new post: ";
                String post;
                cin >> post;
                teacher.setPost(post);
                break;
            }

            case 13: {
                cout << "Enter new science degree: ";
                String scidegree;
                cin.getline(scidegree, 256);
                teacher.setSciDegree(scidegree);
                break;
            }

            case 14: {
                cout << "Enter new qualify: ";
                String qualify;
                cin >> qualify;
                teacher.setQualify(qualify);
                break;
            }

            case 15: {
                cout << "Enter new number of science works: ";
                int num;
                if (!(cin >> num) || num < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect number of science works!" << endl;
                    break;
                }
                String *works = new String[num];
                for (int i=0; i<num; i++) {
                    clear_stream(cin);
                    cout << "Enter work [" << i << "]: ";
                    cin >> works[i];
                }
                teacher.setSciWorks(works, num);
                delete[] works;
                break;
            }

            case 16: {
                cout << teacher << endl;
                break;
            }


            //    PARTY MEMBER !!!
            case 17: {
                try {
                    cin >> member;
                } catch (Human::exBadNumber) {
                    cerr << "Bad year was entered!" << endl;
                    break;
                }

                cout << "Member entered!" << endl;

                PartyMember const who(member);
                cout << "\nWho:\n" << who;

                break;
            }

            case 18: {
                cout << "Enter new name: ";
                String name;
                cin >> name;
                member.setName(name);
                break;
            }

            case 19: {
                cout << "Enter new surname: ";
                String surname;
                cin >> surname;
                member.setSurname(surname);
                break;
            }

            case 20: {
                cout << "Enter new fathername: ";
                String fathername;
                cin >> fathername;
                member.setFathername(fathername);
                break;
            }

            case 21: {
                cout << "Enter new birthyear: ";
                int birth;
                if (!(cin >> birth) || birth < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect birthyear!" << endl;
                    break;
                }
                member.setYear(birth);
                break;
            }

            case 22: {
                cout << "Enter new party name: ";
                String pname;
                cin >> pname;
                member.setPartyName(pname);
                break;
            }

            case 23: {
                cout << "Enter new year of join: ";
                int join;
                if (!(cin >> join) || join < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect year of join!" << endl;
                    break;
                }
                member.setJoin(join);
                break;
            }

            case 24: {
                cout << "Enter new number of party ticket: ";
                int num;
                if (!(cin >> num) || num < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect number of party ticket!" << endl;
                    break;
                }
                member.setTicketNum(num);
                break;
            }

            case 25: {
                cout << "Enter new number of lines in autobiography: ";
                int lines = 0;
                if (!(cin >> lines) || lines < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect number of lines in autobio!" << endl;
                    break;
                }

                String *autobio = new String[lines];
                for (int i=0; i<lines; i++) {
                    clear_stream(cin);
                    cout << "Enter autobio [" << i << "]: ";
                    autobio[i] = new char[500];
                    cin.getline(autobio[i], 500);
                }

                member.setAutobio(autobio, lines);
                delete[] autobio;
                break;
            }

            case 26: {
                cout << member << endl;
                break;
            }



            //    PARTY MEMBER & TEACHER!!!
            case 27: {
                try {
                    cin >> pt;
                } catch (Human::exBadNumber) {
                    cerr << "Bad year was entered!" << endl;
                    break;
                }

                cout << "Party teacher entered!" << endl;

                const PartyTeacher who(pt);
                cout << "\nWho:\n" << who;
                break;
            }

            case 28: {
                cout << "Enter new name: ";
                String name;
                cin >> name;
                pt.PartyMember::setName(name);
                break;
            }

            case 29: {
                cout << "Enter new surname: ";
                String surname;
                cin >> surname;
                pt.PartyMember::setSurname(surname);
                break;
            }

            case 30: {
                cout << "Enter new fathername: ";
                String fathername;
                cin >> fathername;
                pt.PartyMember::setFathername(fathername);
                break;
            }

            case 31: {
                cout << "Enter new birthyear: ";
                int birth;
                if (!(cin >> birth) || birth < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect birthyear!" << endl;
                    break;
                }
                pt.PartyMember::setYear(birth);
                break;
            }

            case 32: {
                cout << "Enter new number of works for party: ";
                int num = 0;
                if (!(cin >> num) || num < 0) {
                    clear_stream(cin);
                    cerr << "Incorrect number of works!" << endl;
                    break;
                }

                String *works = new String[num];
                clear_stream(cin);
                for (int i=0; i<num; i++) {
                    cout << "Enter work [" << i << "]: ";
                    cin >> works[i];
                }
                pt.setWorksForParty(works, num);
                delete[] works;
                break;
            }

            case 33: {
                cout << pt << endl;
                break;
            }

            default:
                break;

        }
    } while (key < 34);

    return 0;
}
