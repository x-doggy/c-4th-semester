#include "xtext.hpp"


Text::Text(): length(0) {
    pt = pb = new TextElem;
    pb->next = pb;
}

Text::Text(char const *str, bool const flag = true): length(0) {
    if (!str)
        throw exBadString();

    pt = pb = new TextElem;
    pb->next = pb;

    if (!flag) {
        ins_begin(str);
    } else {
        char *token = strtok((char *) str, "\t ");
        if (!token) {
            ins_end(str);
        } else {
            ins_end(token);
            while (token = strtok(nullptr, "\t "))
                ins_end(token);
        }
    }
}

Text::Text(const String &str): length(0) {
    pt = pb = new TextElem;
    pb->next = pb;
    ins_begin(str);
}

Text::Text(const Text &text): length(text.length) {
    pt = pb = new TextElem;
    pb->next = pb;

    for (TextElem *p=text.pb->next; p!=text.pb; p=p->next)
        ins_end(p->data);

    pt = pb;
}

Text::Text(Text &&text): length(text.length) {
    std::swap(pb, text.pb);
    std::swap(pt, text.pt);
    text.pb = nullptr;
    text.pt = nullptr;
}

Text::~Text() {
    erase();
    delete pb;
    pt = nullptr;
}



int Text::len() const {
    return length;
}

int Text::slen() const {
    int k = 0;
    for (TextElem *p=pb->next; p!=pb; p=p->next)
        k += p->data.length();
    return k;
}


String& Text::data() const {
    return pt->next->data;
}

String& Text::data(int n) const {
    if (n >= length || n < 0)
        throw exBadIndex(n);

    TextElem *p = pb->next;
    for (int i=0; i<n; i++)
        p = p->next;
    return p->data;
}



void Text::data(int n, String &string) {
    if (n-1 > length || n < 0)
        throw exBadIndex(n);

    pt = pb->next;
    for (int i=0; i<n; i++)
        pt = pt->next;
    pt->data = string;
}

void Text::data(const String &string) {
    if (is_list_end()) return;
    pt->next->data = string;
}



Text* Text::copy() const {
    return new Text(*this);
}

void Text::print(std::ostream &out) const {
    String split = pt == pb ? "|" : "";
    out << split << "BUF" << split;
    for (TextElem *p=pb->next; p!=pb; p=p->next) {
        split = p == pt ? "|" : "";
        out << "->\"" << split << p->data << split << "\"";
    }
}

void Text::erase() {
    if (is_list_empty()) return;
    to_begin();
    while (!is_list_empty()) {
        del_curr();
        to_next();
    }
    to_begin();
}

bool Text::is_list_empty() const {
    return pb->next == pb;
}

bool Text::is_list_end() const {
    return pt->next == pb;
}

void Text::to_begin() {
    pt = pb;
}

void Text::to_next() {
    pt = pt->next;
}

void Text::to_end() {
    if (is_list_end()) return;
    for (TextElem *p=pb->next; !is_list_end(); p=p->next)
        to_next();
}



void Text::ins_middle(int n, const String &string) {
    if (n-1 > length || n < 1) throw exBadIndex(n);

    to_begin();
    for (int i=0; i<n-1; i++)
        to_next();

    ins_curr(string);
}

void Text::ins_begin(const String &string) {
    to_begin();
    ins_curr(string);
}

void Text::ins_end(const String &string) {
    to_end();
    ins_curr(string);
}

void Text::ins_curr(const String &string) {
    TextElem *elem = new TextElem;
    elem->data = string;
    elem->next = pt->next;
    pt = pt->next = elem;
    length++;
}



void Text::ins_middle(int n, const char *string) {
    if (n-1 > length || n < 1) throw exBadIndex(n);
    for (int i=0; i<n; i++)
        to_next();
    ins_curr(string);
}

void Text::ins_begin(const char *string) {
    to_begin();
    ins_curr(string);
}

void Text::ins_end(const char *string) {
    to_end();
    ins_curr(string);
}

void Text::ins_curr(const char *string) {
    if (!string)
        throw exBadString();

    TextElem *elem = new TextElem;
    elem->data = string;
    elem->next = pt->next;
    pt = pt->next = elem;
    length++;
}



void Text::del_middle(int n) {
    if (n-1 > length || n < 1)
        throw exBadIndex(n);

    for (int i=0; i<n; i++)
        to_next();
    del_curr();
}

void Text::del_curr() {
    if ( is_list_empty() || is_list_end() ) return;

    TextElem *elem = pt->next;
    pt->next = elem->next;
    delete elem;
    length--;
}

Text& Text::operator=(const Text &text) {
    if (this != &text) {
        erase();
        for (TextElem *p=text.pb->next; p!=text.pb; p=p->next)
            ins_end(p->data);
        pt = pb;
    }
    return *this;
}

Text& Text::operator=(Text &&text) {
    if (this != &text) {
        length = text.length;
        std::swap(pb, text.pb);
        std::swap(pt, text.pt);
        text.pb = nullptr;
        text.pt = nullptr;
    }
    return *this;
}

std::ostream& operator<<(std::ostream &out, const Text &text) {
    text.print(out);
    return out;
}


// Is texts equal?
bool isTextsEqual(Text &t1, Text &t2) {
    if (t1.is_list_empty() && t2.is_list_empty())
        return true;
    if (t1.len() != t2.len())
        return false;

    TextIterator p1(t1),
                 p2(t2);

    for (int i=0; i<t1.len(); i++) {
        if (p2.get().length() != p1.get().length())
            return false;
        if (p1.get().c_str()[i] != p2.get().c_str()[i])
            return false;

        p1.next();
        p2.next();
    }

    return true;
}




TextIterator::TextIterator(Text &text) : text_(&text) {
    start();
}

void TextIterator::start() {
    text_->to_begin();
}

const String& TextIterator::get() const {
    return text_->data();
}

void TextIterator::next() {
    text_->to_next();
}

bool TextIterator::is_finish() const {
    return text_->is_list_end();
}
