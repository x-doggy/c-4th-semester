/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * 6. To implementate class "text". Text saved as a list of strings.

 * Sizes of text may change dynamically. Methods:
 * Add a string to the head of the text
 * Add a string to the tail of the text
 * Add a string after the n-th element;
 * Get the n-th element;
 * To change the n-th element;
 * To remove the n-th element;
 * Get the number of lines in the text;
 * Get the number of characters in the text;
 * Delete the entire text.

 * Also make a func comparing to texts equals literally.
 */

#include "xtext.hpp"
using namespace std;

inline void menu() {
    cout << "-----------------------------------\n"
         << " 1. Many strings constructor\n"
         << " 2. Copy constructor\n"
         << " 3. C-string constructor\n"
         << " 4. My String constructor\n"
         << " 6. Insert elem to begin\n"
         << " 7. Insert elem to end\n"
         << " 8. Insert elem after current\n"
         << " 9. Insert elem to position\n"
         << "10. Delete current\n"
         << "11. Erase whole list\n"
         << "12. To begin\n"
         << "13. To next\n"
         << "14. To end\n"
         << "15. Is texts equal?\n"
         << "16. Operator []\n"
         << "17. Change current data\n"
         << "18. Change list\n"
         << "-----------------------------------\n\n";
}

inline void clear_stream(istream &s = cin) {
    s.clear();
    s.ignore(numeric_limits<streamsize>::max(), '\n');
}


int main(int argc, char **argv) {
    int key;
    Text s, t, *pText = &s;

    do {
        cout << "\n-----------------------------------\n"
             << (pText == &s ? "---> " : "     ")
             << "s [" << s.len() << ", " << s.slen() << "]: " << s << endl
             << (pText == &t ? "---> " : "     ")
             << "t [" << t.len() << ", " << t.slen() << "]: " << t << endl;

        menu();

        cout << "Enter your choose: ";
        if (!(cin >> key)) {
            clear_stream();
            cerr << "\nIncorrect symbol was entered!" << endl;
            continue;
        }

        switch (key) {
            case 1: {
                cout << "Enter string: ";
                cin.ignore();
                char str[256];
                cin.getline(str, 256);
                Text text(str, true);
                *pText = text;
                break;
            }
            case 2: {
                cout << "Checking copy..." << endl;
                Text *pOther = pText == &s ? &t : &s;
                *pText = *pOther;
                break;
            }
            case 3: {
                cout << "Enter string: ";
                cin.ignore();
                char str[256];
                cin.getline(str, 256);
                Text r(str, false);
                *pText = r;
                break;
            }
            case 4: {
                cout << "Enter string: ";
                cin.ignore();
                char my_c_str[256];
                cin.getline(my_c_str, 256);
                String mystr = my_c_str;
                Text r(my_c_str);
                *pText = r;
                break;
            }
            case 6: {
                cout << "Enter string to insert: ";
                cin.ignore();
                char str[256];
                cin.getline(str, 256);
                pText->ins_begin(str);
                break;
            }
            case 7: {
                cout << "Enter string to insert: ";
                cin.ignore();
                char str[256];
                cin.getline(str, 256);
                pText->ins_end(str);
                break;
            }
            case 8: {
                cout << "Enter string to insert: ";
                cin.ignore();
                char str[256];
                cin.getline(str, 256);
                pText->ins_curr(str);
                break;
            }
            case 9: {
                cout << "Enter string to insert: ";
                char str[256];
                cin.ignore();
                cin.getline(str, 256);
                cout << "In what position to insert: ";
                int k;
                if (!(cin >> k)) {
                    clear_stream();
                    cerr << "Incorrect array\'s size!" << endl;
                    break;
                }

                try {
                    pText->ins_middle(k, str);
                } catch (Text::exBadIndex const &err) {
                    cerr << "Bad index was entered!" << endl;
                    break;
                }
                break;
            }
            case 10: {
                pText->del_curr();
                break;
            }
            case 11: {
                pText->erase();
                break;
            }
            case 12: {
                pText->to_begin();
                break;
            }
            case 13: {
                pText->to_next();
                break;
            }
            case 14: {
                pText->to_end();
                break;
            }
            case 15: {
                cout << "Is texts t&s equal? - " << isTextsEqual(s, t) << endl;
                break;
            }
            case 16: {
                String &&str = (String &&) pText->data();
                cout << "Enter index: ";
                clear_stream();
                int i;
                if (!(cin >> i)) {
                    clear_stream();
                    cerr << "Incorrect array\'s size!" << endl;
                    break;
                }
                char c;
                try {
                    c = str[i];
                    cout << "c = " << c << endl;
                } catch (String::exStringOutOfRange const &err) {
                    cerr << "Error string out of range" << endl;
                    break;
                }
                break;
            }
            case 17: {
                cout << "Enter string: ";
                clear_stream(cin);
                char str[256];
                cin.getline(str, 256);
                pText->data(str);
                break;
            }
            case 18: {
                pText = pText == &s ? &t : &s;
                break;
            }
            default:
                break;
        }

    } while (key < 19);

    return 0;
}
