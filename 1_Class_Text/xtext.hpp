/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Linked-list of strings
 */


#ifndef __X_TEXT__
#define __X_TEXT__

#include <limits>
#include "../cpp_string/cpp_string.h"


struct TextElem {
    String data;
    TextElem *next;

public:
    TextElem(): next(nullptr) {
        data = "";
    }
    TextElem(const TextElem &te): next(te.next) {
        data = te.data;
    }
};


class Text {
    TextElem *pb, *pt;   // pb is a buffer elem, pt is a current elem
    int length;          // number of elements in list

public:
    struct exBadString{};
    struct exBadIndex {
        exBadIndex(int i) : index(i) {}
    private:
        int index;
    };

    // Main constructors
    Text();
    Text(const String&);
    Text(const Text&);
    Text(Text&&);
    ~Text();

    // The possibility of interpreted differently input string.
    // If the Boolean value false, then the list will comprise one element
    // of the input string with spaces (BUF-> "..." ->).
    // Otherwise, the list will consist of units
    // derived from a line division of spaces and tabs
    // (BUF-> "..." -> ... -> "..." ->).
    Text(char const*, bool);

    // Get
    int       len()      const;  // amount of strings
    int       slen()     const;  // amount of symbols
    String&   data(int)  const;  // nth-data elem
    String&   data()     const;  // current data elem

    // Set
    void data(int, String&);
    void data(const String&);

    // Delete methods
    void del_middle(int);
    void del_curr();

    // Specific methods
    Text* copy() const;
    void print(std::ostream&) const;
    void erase();
    bool is_list_empty() const;
    bool is_list_end()   const;
    void to_begin();
    void to_next();
    void to_end();

    // Insert a String object in the middle of list
    void ins_middle(int, const String&);
    void ins_begin(const String&);
    void ins_end(const String&);
    void ins_curr(const String&);

    // The same thing with a char* object
    void ins_middle(int, const char*);
    void ins_begin(const char*);
    void ins_end(const char*);
    void ins_curr(const char*);

    // Operators
    Text& operator=(const Text&);
    Text& operator=(Text&&);
    
    friend std::ostream& operator<<(std::ostream&, const Text&);
    friend class TextBaseIterator;
};


// Is texts equal?
bool isTextsEqual(Text&, Text&);




// Text Iterator
class TextBaseIterator {
public:
    virtual ~TextBaseIterator() {}
    virtual void start() = 0;
    virtual const String& get() const = 0;
    virtual void next() = 0;
    virtual bool is_finish() const = 0;
};


class TextIterator : public TextBaseIterator {
    Text *text_;

public:
    TextIterator(Text &text);
    void start();
    const String& get() const;
    void next();
    bool is_finish() const;
};


#endif
